(ns matchstick.modifiers.explode
  (:require
    [thi.ng.geom.vector :refer [vec3]]))

;; ======== HELPERS ===========
(def is-compiled []
  )


(defn explode [spec]
  "Make all faces use unique vertices so that each face can be separated from others.
  Ported from Three.js implementation by @alteredq. Expects a map with the keys vertices and indices
  and returns a map of the same with the processed information."
  (let[vert-set (atom [])
       index-set (atom [])
       vertices (:vertices spec)
       indexSet (partition 3 (:indices spec))]
    (take-last 1 (map-indexed (fn [i iSet]

                                (let [n (count @vert-set)
                                      a (nth iSet 0)
                                      b (nth iSet 1)
                                      c (nth iSet 2)
                                      va (nth vertices a)
                                      vb (nth vertices b)
                                      vc (nth vertices c)]


                                  (swap! vert-set conj va vb vc)
                                  (swap! index-set conj n (+ n 1) (+ n 2))
                                  )


                                ) indexSet))
    {:vertices @vert-set
     :indices @index-set}
    )
  )
