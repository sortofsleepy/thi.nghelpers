(ns audivisi.soundcloud
  (:require
    [ajax.core :refer [GET]]))
(enable-console-print!)

(def client_id "b36fa0e8aa765329c8bad7c78e01b31a")
(def baseurl "https://api.soundcloud.com/resolve.json")
(defn get-data
  "Gets JSON information from Soundcloud about the specified url"
  ([goal cb]
   (let [url (str baseurl "?client_id=" client_id "&url=" goal)]
     (GET url {:handler cb
               :error-handler
                        (fn [e]
                          (js/console.error "Issue retrieving data" e))}))))


(defn getSong [song cb]
  "Retrieves song information from Soundcloud. Returns an audio tag with the
  audio, as well as the streamurl and title "
  (get-data
    song
    (fn [e]
      (let [streamurl (get e "stream_url")
            title (get e "title")
            attachments (get e "attachments_uri")
            audio (js/Audio.)]
        (aset audio "crossOrigin" "Anonymous")
        (aset audio "src" streamurl)
      (cb audio streamurl title attachments)))))