(ns matchstick.core.mesh
  (:require
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.glmesh :as glm]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.typedarrays.core :as arrays]))

(defn CreateIndexedMesh
  ([data]
   "Builds an IndexedGLMesh. Accepts a Map with the props :vertices :normals, :uvs and :indices"
   (let [model (-> (glm/IndexedGLMesh.
                     (:vertices data)
                     (:normals data)
                     (:normals data)
                     (:uvs data)
                     (arrays/float32 (* 28))
                     (:indices data)
                     #{:uvs :fnorm :vnorm}
                     {} 0 0)
                   (gl/as-gl-buffer-spec {:num-items    6
                                             :num-vertices 27
                                             :mode         glc/triangles}))
         ]))

  ([vertices uvs indices]
   (let [flatverts  (arrays/float32 (flatten vertices))
         model (-> (glm/IndexedGLMesh.
                    flatverts
                     nil
                     nil
                     (arrays/float32 (flatten uvs))
                     nil
                     (arrays/uint16 (flatten indices))
                     #{:uvs}
                     {} (/ (count flatverts) 3)
                     (count (flatten indices))))
         ]
     model))
  )
