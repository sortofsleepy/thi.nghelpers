(ns matchstick.core.camera
  (:require
    [thi.ng.geom.gl.arcball :as arc]
    [thi.ng.geom.gl.camera :as cam]
    [thi.ng.geom.rect :as r]
    [thi.ng.math.core :as m]
    [thi.ng.geom.quaternion :as q]
    [thi.ng.geom.core :as g]
    [thi.ng.geom.vector :as v]))
(enable-console-print!)



(defn camera-translate [camera translateVector]
  "Helper function to make translation of the camera a little more understandable"
  (let [ucam (cam/set-view camera {:eye translateVector})]
    ucam))

(defn camera-updateprojection [camera view-rect]
  "Sets a new projection matrix based on the specified viewport rect"
  (let [c (cam/set-projection camera {:aspect view-rect})]
    c))


(defprotocol Camera
  (init [this])
  (applyTo [this model])
  (enableArcball [this])
  (setEye [this translateVec])
  (updateProjection [this width height]))

(defrecord PerspectiveCamera [options]
  Camera
  (init [this]
    (aset this "state" (atom {:camera (cam/perspective-camera options)
                              :vrect (r/rect 0 0 js/window.innerWIdth js/window.innerHeight)
                              :using-arcball false
                              :proj nil
                              :mouse-down false}))

    ;; store the computed projection matrix from the camera helper
    (let [state (aget this "state")
          cam (:camera @state)]
      (swap! state assoc :proj (get cam :proj)))
    this)

  (applyTo [this spec]
    "Applies projection and view matrices to a model"
    (let [state (aget this "state")
          usingArcball (:using-arcball @state)
          cam (:camera @state)]

      (if (= usingArcball true)
        ()
        (update spec :uniforms merge {:view (get cam :view) :proj (get cam :proj)}))

      ))


  (updateProjection [this width height]
    "Updates the projection matrix for the camera"
    (let [rect (r/rect 0 0 width height)
          state (aget this "state")
          camera (:camera @state)
          c (cam/set-projection camera {:aspect rect})]
      ;; update the current viewing rect
      (swap! state assoc :vrect rect)
      ;; updat ethe current camera object
      (swap! state assoc :camera c)

      ;; update stored camera projection
      (swap! state assoc :proj (get camera :proj))
      ) this)


  (enableArcball [this]

    (let [state (aget this "state")
          vrect (:vrect @state)
          cam (:camera @state)]
      (swap! state assoc :using-arcball true)
      (swap! state assoc :camera
             (-> (arc/arcball {:init (m/normalize (q/quat 0.0 0.707 0.707 0))})
                 (arc/resize (g/width vrect) (g/height vrect))))
      (doto js/document
        (.addEventListener
          "mousedown"
          (fn [e]
            (doto state
              (swap! assoc :mouse-down true)
              (swap! update :camera arc/down (.-clientX e) (.-clientY e)))))
        (.addEventListener
          "mouseup"
          (fn [e] (swap! state assoc :mouse-down false)))
        (.addEventListener
          "mousemove"
          (fn [e]
            (when (:mouse-down @state)
              (swap! state update :camera arc/drag (.-clientX e) (.-clientY e)))))))
    )

  (setEye [this translateVec]
    "Sets the position of the camera"
    (let [state (aget this "state")
          camera (:camera @state)]
      (swap! state assoc :camera (cam/set-view camera {:eye translateVec}))
      ))
  )

(defn make-camera [options]
  (let [camera (PerspectiveCamera. options)]
    (-> camera
        (init))))
