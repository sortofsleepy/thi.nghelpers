(ns matchstick.core.vbomesh
  (:require
    [thi.ng.geom.matrix :refer [M44]]))


(deftype VboMesh [gl]
  Object
  (init [this]
    (aset this "model" (-> M44)))

  (rotateX [this])
  (rotateY [this])
  (rotateZ [this])

  (draw [this]
    ))