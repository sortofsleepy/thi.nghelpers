(ns matchstick.core.instanced-drawing
  (:require
    [clojure.walk :refer [stringify-keys]]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.shaders :as sh]))

(defn enableInstancing [gl]
  (let [ext (.getExtension gl "ANGLE_instanced_arrays")]
    (if (= ext nil)
      (js/console.error "Unable to use instancing on this card")
      (do ext))))

(defn set-attribute
  ([^WebGLRenderingContext gl shader id attrib-spec]
   (let [ext (or ext (enableInstancing gl))
         {:keys [buffer stride size type normalized? offset loc instanced]} attrib-spec]


     (let [loc (int (-> shader (get :attribs) (get id)))]
       (.bindBuffer gl glc/array-buffer buffer)
       (if (not= instanced nil)
         (do
           (.enableVertexAttribArray gl loc)
           (.vertexAttribPointer gl
                                 loc
                                 (int size)
                                 (or type glc/float)
                                 (boolean normalized?)
                                 (or stride 0)
                                 (or offset 0))
           (.vertexAttribDivisorANGLE ext loc instanced))
         (do

           (.enableVertexAttribArray gl loc)
           (.vertexAttribPointer gl
                                 loc
                                 (int size)
                                 (or type glc/float)
                                 (boolean normalized?)
                                 (or stride 0)
                                 (or offset 0))))
       ;;(.vertexAttribDivisorANGLE ext loc 0)
       (.bindBuffer gl glc/array-buffer nil)))))

(defn end-instancing
  "Reset state for non-instanced items"
  ([^WebGLRenderingContext gl shader id attrib-spec]
   (let [ext (enableInstancing gl)
         {:keys [buffer instanced]} attrib-spec]
     (let [loc (int (-> shader (get :attribs) (get id)))]
       (.bindBuffer gl glc/array-buffer buffer)
       (if (not= instanced nil)
         (.vertexAttribDivisorANGLE ext loc 0)
         )
       (.bindBuffer gl glc/array-buffer nil)))))

(defn begin-shader [^WebGLRenderingContext gl shader uniforms attribs indices]
  (let [ext (enableInstancing gl)]
    (.useProgram gl (get shader :program))
    (sh/apply-default-uniforms shader uniforms)
    (reduce-kv #(sh/set-uniform shader uniforms %2 %3) nil uniforms)
    (reduce-kv #(set-attribute gl shader %2 %3) nil attribs)
    (when indices
      (.bindBuffer gl glc/element-array-buffer (get indices :buffer))))

  )

(defn end-shader [^WebGLRenderingContext gl shader uniforms attribs indices]
  (let [ext (enableInstancing gl)]
    (reduce-kv #(end-instancing gl shader %2 %3) nil attribs)
    ))

(defn draw-instanced [gl spec]
  (let [mode (get spec :mode glc/triangles)
        ext (enableInstancing gl)
        numInstances (get spec :num-instances)]

    (if (get spec :indices)
      (.drawElementsInstancedANGLE ext mode (get spec :num-items) glc/unsigned-short 0,numInstances)
      (.drawArraysInstancedANGLE ext mode 0 (get spec :num-vertices) 200)
      )))


;; this works exactly like gl/draw-with-shader but for instanced stuff
(defn instanced-draw [gl {:keys [shader] :as spec}]
  (gl/prepare-render-state gl shader)
  (begin-shader gl shader (get spec :uniforms) (get spec :attribs) (get spec :indices))
  (draw-instanced gl spec)
  (end-shader gl shader (get spec :uniforms) (get spec :attribs) (get spec :indices))
  (gl/end-shader gl shader))
