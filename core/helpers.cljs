(ns matchstick.core.helpers
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.core :as g]))

(defn setCamera [spec camera]
  "Attempts to set camera projection and view matrices.
  If the keys aren't found in the camera parameter, a value of nil is set."
  (let [projection (or (:proj camera) (:projectionMatrix camera))
        view (or (:view camera) (:viewMatrix camera))]
    (update spec :uniforms merge {:view (or view nil) :proj (or projection nil)})))

(defn updateModelMatrix [mesh value]
  "A wrapper around updating the model matrix to make things a little
  more clear. Takes in a built spec and returns the same one with the
  newly updated model."
  (assoc-in mesh [:uniforms :model] value))

(defn uniform [mesh key value]
  "Makes it a little more clear when updating model uniforms."
  (assoc-in mesh [:uniforms key] value))

(defn resetViewport
  "Resets the viewport to the full width and height of the browser"
  ([gl]
   (gl/set-viewport gl 0 0 js/window.innerWidth js/window.innerHeight)))

(defn scaleMesh [mesh scale]
  "Scales the model matrix of a mesh spec. If the mesh doesn't
   already have a model matrix, one gets assigned to it. Might
   not be best in a animation situation but more useful as a step prior
   to drawing mesh in a loop"
  (let [model (get-in mesh [:uniforms :model])]
    (if (not= model nil)
      (assoc-in mesh [:uniforms :model] (-> model (g/scale scale)))
      (assoc-in mesh [:uniforms :model] (-> M44 (g/scale scale))))))

(defn translateMesh [mesh position]
  (let [model (get-in mesh [:uniforms :model])]
    ))