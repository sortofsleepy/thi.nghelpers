(ns matchstick.core.texture
  (:require
    [thi.ng.geom.gl.buffers :as buf]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.math.core :as math]))

(defn make-texture
  "Works the same as thi.ng.geom.gl.buffers/make-texture but doesn't leave
  the texture bound after configuration"
  [^WebGLRenderingContext gl opts]
  (let [tex  (buf/Texture2D. gl (.createTexture gl) (opts :target glc/texture-2d))
        opts (merge {:format glc/rgba :type glc/unsigned-byte} opts)]
    (gl/bind tex)
    (let [configured-tex (gl/configure tex opts)]
      (gl/unbind tex)
      configured-tex
      )))

;; if available, enable floating point textures
;; TODO add reader conditionals for non cljs?
(defn enableFloatExtentions [gl]
  (.getExtension gl "OES_texture_float")
  (.getExtension gl "OES_float_linear"))
;; ========== CORE FUNCTIONS =================
(defn make-datatexture
  "Creates a 128x128 texture for the purposes of holding data "
  ([^WebGLRenderingContext gl data]
   (enableFloatExtentions gl)
   (make-texture gl {:width  128
                     :height 128
                     :filter glc/nearest
                     :wrap   glc/clamp-to-edge
                     :format glc/rgba
                     :type   glc/float
                     :pixels (or (if (vector? data) (arrays/float32 (flatten data))) data)}))
  ([^WebGLRenderingContext gl data size]
   (enableFloatExtentions gl)
   (make-texture gl {:width  size
                     :height size
                     :filter glc/nearest
                     :wrap   glc/clamp-to-edge
                     :format glc/rgba
                     :type   glc/float
                     :pixels (or (if (vector? data) (arrays/float32 (flatten data))) data)}))
  )

(defn make-imagetexture
  ([^WebGLRenderingContext gl url cb]
   (buf/load-texture gl {:callback cb
                         :src url
                         :flip false}))
  ([^WebGLRenderingContext gl url flip cb]
   (buf/load-texture gl {:callback cb
                         :src url
                         :flip flip})))

(defn generateBlankTexture
  "Creates a blank texture of floating point data"
  ([gl size]
   (let [data (arrays/float32 (repeatedly (* size size 4) #(math/random)))]
     (make-datatexture gl data size))))
