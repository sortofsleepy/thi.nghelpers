(ns matchstick.core.utils
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.buffers :as buf]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.color.core :as col]
    [thi.ng.math.core :as math]))
;; ------------------- USEFUL GENERAL FUNCTIONS ----------------------
(defn addUniform [spec uniformMap]
  "Convinience function for adding a map of uniforms to a shader spec"
  (let [newSpec (update-in spec [:uniforms] merge uniformMap)]
    newSpec))

(defn compute-normal-matrix
  "Computes a normal matrix based on the model and view matrices
  passed in as parameters."
  [m v] (-> v (math/* m) (math/invert) (math/transpose)))

(defn BuildSpec
  "named function for build out maps into drawable models"
  ([gl modelMap]
   (-> modelMap (gl/make-buffers-in-spec gl glc/static-draw)))
  ([gl modelMap mode]
   (-> modelMap (gl/make-buffers-in-spec gl mode))))


(defn make-texture
  "Works the same as thi.ng.geom.gl.buffers/make-texture but doesn't leave
  the texture bound after configuration"
  [^WebGLRenderingContext gl opts]
  (let [tex  (buf/Texture2D. gl (.createTexture gl) (opts :target glc/texture-2d))
        opts (merge {:format glc/rgba :type glc/unsigned-byte} opts)]
    (gl/bind tex)
    (let [configured-tex (gl/configure tex opts)]
      (gl/unbind tex)
      configured-tex
      )))

(defn reset-viewport
  ([gl]
   "Useful for when using fbos. Restores the original viewport for the
    context. Defaults to 0 0 <full width of window> <full height of window>"
   (doto gl
     (gl/set-viewport 0 0 js/window.innerWidth js/window.innerHeight)
     (gl/clear-color-and-depth-buffer col/BLACK 1)))

  ([gl clearcolor]
   "Useful for when using fbos. Restores the original viewport for the
    context. Defaults to 0 0 <full width of window> <full height of window>"
   (doto gl
     (gl/set-viewport 0 0 js/window.innerWidth js/window.innerHeight)
     (gl/clear-color-and-depth-buffer clearcolor 1)))


  ([gl x y width height]
   "Useful for when using fbos. Pass in the x/y position for the viewport and
   a width and height for it"
   (doto gl
     (gl/set-viewport x y width height)
     (gl/clear-color-and-depth-buffer col/BLACK 1)))

  ([gl x y width height clearcolor]
   "Useful for when using fbos. Pass in the x/y position for the viewport and
   a width and height for it. Also specify a clear color"
   (doto gl
     (gl/set-viewport x y width height)
     (gl/clear-color-and-depth-buffer clearcolor 1)))
  )


(defn UpdateBuffer [gl spec id coll]
  "Updates the data of the specified attribute in a model spec.
  Takes a context, a spec map, the id of the attribute to update and finally
  the data used in the update."
  (let [{:keys [target data buffer buffer-mode size]} (-> (get spec :model) :attribs id)]
    (gl/fill-vertex-buffer data coll size)
    (.bindBuffer gl target buffer)
    (.bufferData gl target data buffer-mode))
  )

(defn EnableWireframe [spec]
  "Enables wireframe mode for the specified spec. Returns a
  new spec."
  (assoc-in spec [:mode] glc/line-loop))

(defn ReadPixels
  ([gl width height]
   ;; TODO it seems like this is the only way readPixels functions? Tried a bunch of other combos with no luck
   (let [storage (arrays/uint8 (* width height 4))
         x 0
         y 0
         format glc/rgba
         type glc/unsigned-byte]
     (.readPixels gl x y width height format type storage)
     storage)))
;; ------------------- SPECS ----------------------
(defn StarterSpec
  ([]
   "Returns an initial map for building geometry"
   {:attribs      {}
    :mode         glc/triangle-strip
    :num-vertices 20
    :shader       {}})

  ([& attribs]
   (let [base  (atom {:attribs      {}
                      :mode         glc/triangle-strip
                      :num-vertices 20
                      :shader       {}})]

     (doseq [attrib attribs]
       (let [newSet (assoc-in @base [:attribs] attrib)]
         (reset! base newSet)))
     @base)
    )
  )

(defn StarterShaderSpec
  ([vertex fragment]
   "Defines a complete basic color shader spec"
   {:vs vertex
    :fs fragment
    :uniforms {:proj :mat4
               :view :mat4
               :model [:mat4 M44]}
    :attribs  {:position :vec3
               :normal :vec3
               :color :vec3
               :uv :vec2}
    :varying  {}
    :state    {}})
  ( []
   "Defines a complete basic color shader spec"
   {:vs ""
    :fs ""
    :uniforms {:proj :mat4
               :view :mat4
               :model [:mat4 M44]}
    :attribs  {:position :vec3
               :normal :vec3
               :color :vec3
               :uv :vec2}
    :varying  {}
    :state    {}}))
