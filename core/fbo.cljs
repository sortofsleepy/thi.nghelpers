(ns matchstick.core.fbo
  (:require
    [thi.ng.geom.gl.fx :as fx]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [matchstick.core.texture :as mtex]
    [thi.ng.geom.gl.buffers :as buf]
    [matchstick.geometry.plane :as plane]
    [matchstick.shaders.texture-shader :as tshader]))
;; ============= DEFINITION ====================
;; A extention of the FBO in thi.ng/webgl to provide a little more function
(defrecord Fbo [gl texture fbo width height quad]
  Object
  (viewport [this]
    "Convinience function for setting the FBO viewport"
    (gl/set-viewport gl 0 0 width height))

  (setTexture [this tex]
    "Sets a texture on this fbo"
    (gl/set-fbo-color-texture fbo tex))

  (setCamera [this camera]
    "Applies camera matrices to the full screen quad that needs to get drawn"
    (assoc-in quad [:uniforms :proj] (:proj camera))
    (assoc-in quad [:uniforms :view] (:view camera)))

  (drawWithCamera [this camera]
    (gl/draw-with-shader
      gl
      (-> quad
          (.setCamera this camera))))
  (draw [this]
    (gl/draw-with-shader
      gl
      quad))
  gl/IBind
  (bind [this]
    "Binds the fbo for drawing"
    (gl/bind fbo))

  (bind [this unit]
    "Binds the texture of the render target. By
    default, will bind to unit 0 but can pass in
    a unit as well"
    (gl/bind texture (or unit 0)))

  (unbind [this]
    "Unbinds both the texture and the fbo"
    (gl/unbind texture)
    (gl/unbind fbo)))
;; =========== FUNCTION ===============
(defn make-fbo
  "Builds Fbos. One form is for regular fbos to draw "
  ([gl & {:keys [width height filter format wrap] :or {width 512
                                                       height 512
                                                       filter glc/nearest
                                                       format glc/rgba
                                                       wrap glc/clamp-to-edge}}]

   (let [texture (mtex/make-texture gl {:format format
                                        :wrap   wrap
                                        :width  width
                                        :filter filter
                                        :height height})
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width  width
                                                :height height})
         ;; build a quad for drawing the result
         quad (-> (fx/init-fx-quad gl)
                  (assoc :shader (sh/make-shader-from-spec gl fx/shader-spec))
                  (assoc-in [:shader :state :tex] texture))]
     (->Fbo gl texture fbo 512 512 quad)))

  ([^WebGLRenderingContext gl data]
   "Builds a RenderTarget with floating point data. Takes a WebGLRenderingContext and a Map of options for the texture."
   (let [texture (mtex/make-datatexture gl data)
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width 128
                                                :height 128
                                                :depth? false})
         ;; build a quad for drawing the result
         quad (-> (fx/init-fx-quad gl)
                  (assoc :shader (sh/make-shader-from-spec gl fx/shader-spec))
                  (assoc-in [:shader :state :tex] texture))]
     (->Fbo gl texture fbo 128 128 quad)))

  ([^WebGLRenderingContext gl data size]
   "Builds a RenderTarget with floating point data. Takes a WebGLRenderingContext and a Map of options for the texture.
   Also takes a size for sizing the fbo and texture."
   (let [texture (mtex/make-datatexture gl data)
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width size
                                                :height size
                                                :depth? false})
         ;; build a quad for drawing the result
         quad (-> (fx/init-fx-quad gl)
                  (assoc :shader (sh/make-shader-from-spec gl fx/shader-spec))
                  (assoc-in [:shader :state :tex] texture))]
     (->Fbo gl texture fbo size size quad))))



(defn make-float-fbo-with-texture [gl texture size]
  "Builds a RenderTarget with floating point data. Takes a WebGLRenderingContext and a Map of options for the texture."
  (let [fbo (buf/make-fbo-with-attachments gl {:tex texture
                                               :width size
                                               :height size
                                               :depth? false})
        ;; build a quad for drawing the result
        quad (-> (fx/init-fx-quad gl)
                 (assoc :shader (sh/make-shader-from-spec gl fx/shader-spec))
                 (assoc-in [:shader :state :tex] texture))]
    (->Fbo gl texture fbo size size quad)))

(defn make-quad [gl width height]
  "Makes a quad that can be used for rendering FBO contents to screen"
  (let [data (plane/CreatePlaneVertices width height 2 2)
        verts (:vertices data)
        uvs (:uvs data)
        normals (:normals data)
        indices (:indices data)
        spec {:attribs   {:position {:data verts
                                     :size 3}
                          :uv       {:data uvs
                                     :size 2}}
              :mode      glc/triangles
              :num-items (aget indices "length")
              :indices   {:data indices}
              :shader    (sh/make-shader-from-spec gl tshader/spec)}
        ]
    (-> spec (gl/make-buffers-in-spec gl glc/dynamic-draw))))