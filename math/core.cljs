(ns matchstick.math.core
  (:require
    [thi.ng.math.core :as math]))
(defn toDegrees [rad]
  "Converts radians to degrees"
  (/ (* rad 180) math/PI))

(defn toRadians [deg]
  "Converts degrees to radians"
  (/ (* deg math/PI) 180))

(defn lerp [val min max]
  "Returns the value linear interpolated"
  (+ min (* val (- max min))))

(defn randRange [min max]
  "Returns a random number between a range"
  (+ min (* (rand) (- max min))))

(defn constrain [v min max]
  "Constrains a value in between a min and max"
  (let [val (atom 0)]
    (if (< v min)
      (reset! val min))
    (if (> v max)
      (reset! val max))

    (if (= @val 0)
      (reset! val v))
    @val))
