(ns matchstick.shaders.emitter-shader
  (:require [thi.ng.geom.vector :as v]
            [thi.ng.geom.matrix :refer [M44]]
            [matchstick.shaders.noise.curlnoise :as cnoise]))

;; demonstrates a basic curl noise shader

(def simshader
  {:vs       (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vUv = uv;"
                  "gl_Position =  model * vec4(position, 1.0);"
                  "}")

   :fs       (str
               cnoise/curlNoise
               "void main (){"
               "vec2 uv = gl_FragCoord.xy / resolution;"
               "vec4 oPos = texture2D( originTexture , vUv );"
               "vec4 pos  = texture2D( destinationTexture , vUv );"
               "vec3 vel = pos.xyz - oPos.xyz;"

               "vec3 curl = curlNoise( pos.xyz * noiseSize );"

               "vel += curl * 0.01;"
               "vel *= .97;"

               "vec3 p = pos.xyz + vel;"
               ;;"vec3 p = oPos.xyz;"

               "gl_FragColor = vec4( p , 1. );"
               ;;"gl_FragColor = vec4(1.0,1.0,0.0,1.0);"
               "}")
   :uniforms {:proj               :mat4
              :model              [:mat4 M44]
              :noiseSize          [:float 0.1]
              :resolution         [:vec2 (v/vec2 js/window.innerWidth js/window.innerHeight)]
              :originTexture      [:sampler2D 1]
              :destinationTexture [:sampler2D 2]}
   :attribs  {:position :vec3
              :normal   :vec3
              :color    :vec3
              :uv       :vec2}
   :varying  {:vUv :vec2}
   :state    {:depth-test false}})

;; --------------- RENDERING SHADER -----------------
(def rendershader
  "Defines the rendering shader used in the render stage of ping-ponging
  set the texture uniform with the texture that you expect to receive from the simulation state"
  {:vs       (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vUv = uv;"
                  "vPosition = position;"
                  "vec4 pos = texture2D( tpos , position.xy );"

                  "gl_Position =  proj * view * model * vec4(pos.xyz, 1.0);"
                  "}")
   :fs       (str "void main() {
                vec2 uv = gl_FragCoord.xy / resolution;
                //gl_FragColor = vec4(uv,0.0,1.0);
                 vec4 oPos = texture2D(tpos,vUv);

                if(vPosition.y == 0.0){
                  gl_FragColor = vec4(uv,0.0,0.0);
                }else{
                  gl_FragColor = vec4(uv,0.0,1.0);
                  }
             }")

   :uniforms {:proj       :mat4
              :view       :mat4
              :model      [:mat4 M44]
              :resolution [:vec2 (v/vec2 js/window.innerWidth js/window.innerHeight)]
              :tpos       [:sampler2D 0]}
   :attribs  {:position :vec3
              :normal   :vec3
              :color    :vec3
              :uv       :vec2}
   :varying  {:vUv       :vec2
              :vPosition :vec3}
   :state    {}})
