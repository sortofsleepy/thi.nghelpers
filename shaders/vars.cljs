(ns matchstick.shaders.vars)

;; defines frag string for basic yello color. Primarily for debugging
(def frag_yellow_color "gl_FragColor = vec4(1.0,1.0,0.0,1.0);")

;; defines a default uniform map of uniforms generally used by most shaders
(def defaultUniformMap { :proj :mat4
                          :view :mat4
                          :time :float
                          :model :mat4})

;; defines a map of basic attributes generally used by most shaders
(def defaultAttributemMap  {:position :vec3
                            :normal :vec3
                            :color :vec3
                            :uv :vec2})

;; basic passthru variables that need to be declared even when not in use
(def passthur_vars "vec3 c = color;\n vec3 n = normal;\n vec2 u = uv;\"" )

;; basic passthru call
(def passthru_position (str "gl_Position =  proj * view * model * vec4(position, 1.0);"))

(def spec
  "Format for a shader"
  {:vs ""
   :fs ""
   :uniforms {:proj :mat4
              :view :mat4
              :time :float
              :model :mat4}
   :attribs  {:position :vec3
              :normal :vec3
              :color :vec3
              :uv :vec2}
   :varying  {}
   :state    {}})
