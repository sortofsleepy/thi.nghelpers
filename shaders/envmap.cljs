(ns matchstick.shaders.envmap
  (:require
    [thi.ng.geom.matrix :refer [M44]]))
(def vertex
  "


  float getDepth(float z, float n, float f){
    return (2.0 * n) / (f + n - z*(f-n));
  }

  vec4 quat_from_axis_angle(vec3 axis,float angle){
    vec4 qr;
    float half_angle = (angle * 0.5) * 3.14159 / 180.0;
    qr.x = axis.x * sin(half_angle);
    qr.y = axis.y * sin(half_angle);
    qr.z = axis.z * sin(half_angle);
    qr.w = cos(half_angle);
    return qr;
  }


vec3 rotate_vertex_position(vec3 position, vec3 axis, float angle) {
 vec4 q = quat_from_axis_angle(axis, angle);
 vec3 v = position.xyz;
 return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
 }

 void main(){
    vec3 pos = rotate_vertex_position(position, axis, angle);
    vec4 V = proj * (modelViewMatrix * vec4(pos, 1.0));
    gl_Position = V;
    vUv = uv;
    float d       = getDepth(V.z/V.w, near, far);
    vDepth        = d;
 }
  ")
(def fragment
  "
  const vec3 FOG_COLOR = vec3(243.0, 230.0, 214.0)/255.0;
  void main(){
    //vec4 color = texture2D(texture, vUv);
    gl_FragColor = vec4(1.0,1.0,0.0,1.0);
  }
  ")

(defn generateEnvMapShader []
  "Returns a compiled spec for env map geometry"
  {:vs vertex
   :fs fragment
   :uniforms {:modelViewMatrix [:mat4 M44]
              :proj :mat4
              :view [:mat4 M44]
              :axis [:vec3 [0.9170600771903992, 0.39874908328056335, 0]]
              :angle :float
              :near [:float 0.1]
              :far [:float 10000.0]}
   :attribs  {:position :vec3
              :uv       :vec2}
   :varying  {:vUv       :vec2
              :vDepth :float}
   })