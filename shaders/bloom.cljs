(ns matchstick.shaders.bloom
  (:require-macros
    [macros.helpers :refer [loadfile]]))


(def vertex "src/matchstick/shaders/source/bloom.vert")
(def fragment "src/matchstick/shaders/source/bloom.frag")


(def spec
  {:vs vertex
   :fs fragment
   :uniforms {:model :mat4
              :proj :mat4
              :view :mat4
              :viewVector :vec3
              :glowColor :vec3
              :c :float
              :p :float}
   :varying {:intensity :float}
   :attribs {:position :vec3}})
