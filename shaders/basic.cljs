(ns matchstick.shaders.basicshaders
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [matchstick.shaders.vars :as svars]))



;;---------------- BASIC PASSTHRU VERTEX SHADER ----------------------

;; basic passthru vertex shader
(def basic-passthru
  (str "void main() {
         vec3 c = color;
         vec3 n = normal;
         vec2 u = uv;"
       svars/passthru_position
       "}"))

(def basic-passthru-fragment
  (str "void main() {
         gl_FragColor = vec4(1.0,0.0,0.0,1.0);"
       "}"))

;;---------------- BASIC COLOR SHADER ----------------------
(defn basic-shader
  ([r g b]
   "Defines a complete basic color shader spec. Yellow is the default
color used in the fragment"
   {:vs (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vec2 u = uv;"
             svars/passthru_position
             "}")
    :fs (str "void main() {
                gl_FragColor = vec4(" r "," g "," b ",1.0);
             }")
    :uniforms {:proj :mat4
               :view :mat4
               :time :float
               :ocolor :vec3
               :model [:mat4 M44]}
    :attribs  {:position :vec3
               :normal :vec3
               :color :vec3
               :uv :vec2}
    :varying  {}
    :state    {:depth-test true}})

  ([]
   "Defines a complete basic color shader spec. Yellow is the default
   color used in the fragment"
   {:vs (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vec2 u = uv;"
             svars/passthru_position
             "}")
    :fs (str "void main() {
                gl_FragColor = vec4(1.0,1.0,0.0,1.0);
             }")
    :uniforms {:proj :mat4
               :view :mat4
               :time :float
               :ocolor :vec3
               :model [:mat4 M44]}
    :attribs  {:position :vec3
               :normal :vec3
               :color :vec3
               :uv :vec2}
    :varying  {}
    :state    {:depth-test true}}))


(defn basic-texture-shader
  ([]
   "Defines a complete basic color shader spec. Yellow is the default
 color used in the fragment"
   {:vs (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vUv = uv;"
             svars/passthru_position
             "}")
    :fs (str "void main() {
                vec4 tex = texture2D(texture,vUv);
                gl_FragColor = tex;
             }")
    :uniforms {:proj :mat4
               :view :mat4
               :time :float
               :ocolor :vec3
               :texture :sampler2D
               :model [:mat4 M44]}
    :attribs  {:position :vec3
               :normal :vec3
               :color :vec3
               :uv :vec2}
    :varying  {:vUv :vec2}
    :state    {}}))
