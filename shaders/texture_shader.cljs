(ns matchstick.shaders.texture-shader
  (:require
    [thi.ng.geom.matrix :refer [M44]]))

;; A bearbones shader used to render something that
;; requires a texture.

(def vertex
  "void main () {
    vUv = uv;
    gl_Position = proj * view * model * vec4(position,1.);
   }")

(def fragment
  "void main(){
    vec4 tex = texture2D(texture,vUv);
    gl_FragColor = tex;
    //gl_FragColor = vec4(1.0,1.0,0.0,1.0);
  }")

(def spec
  {:vs vertex
   :fs fragment
   :uniforms {:proj :mat4
              :view :mat4
              :model [:mat4 M44]
              :texture [:sampler2D 0]}
   :attribs {:position :vec3
             :uv :vec2}
   :varying {:vUv :vec2}})