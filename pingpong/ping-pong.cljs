(ns matchstick.core.ping-pong
  (:require
    [matchstick.core.texture :as tex]
    [matchstick.geometry.primitives :as prim]
    [matchstick.core.fbo :as fbo]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.typedarrays.core :as arrays]))

(defn buildRect [gl shader]
  (let [modelPrep {:attribs      {:position {:data (arrays/float32 [-1 -1 1 -1 -1 1 1 1]) :size 2}}
                   :mode         glc/triangle-strip
                   :shader (sh/make-shader-from-spec gl shader)
                   :num-vertices 4}]
    (-> modelPrep (gl/make-buffers-in-spec gl glc/dynamic-draw))))

(deftype PingpongBuffer [gl simulation size]
  Object
  (init [this]
    (aset this "rt1" (fbo/make-float-fbo-with-texture gl (tex/generateBlankTexture gl size) size))
    (aset this "rt2" (fbo/make-float-fbo-with-texture gl (tex/generateBlankTexture gl size) size))
    (aset this "size" (.getSize (aget this "rt1")))
    (aset this "quad" (buildRect gl simulation))
    (aset this "tmp" (aget this "rt2")) this)

  (setData [this data]
    "Sets origin data onto the fbos"
    (let [rt1 (aget this "rt1")
          rt2 (aget this "rt2")
          origin (tex/make-datatexture gl data)]
      (.setTexture rt1 origin)
      (.setTexture rt2 origin)))

  (getOutput [this]
    "Returns the current output for the ping-pong process"
    (:texture (aget this "rt2")))

  (bindOutput [this unit]
    (let [out (aget this "rt2")]
      (gl/bind out unit)))

  (swap [this]
    "Swaps buffers so that buffer 1 becomes buffer 2, etc. on each pass"
    (aset this "tmp" (aget this "rt2"))
    (aset this "rt2" (aget this "rt1"))
    (aset this "rt1" (aget this "tmp")))

  (update [this]
    "Updates the simulation"
    (let [rt1 (aget this "rt1")
          rt2 (aget this "rt2")
          quad (aget this "quad")
          size (aget this "size")]

      ;; bind main fbo
      (gl/bind rt1)
      (gl/disable gl glc/depth-test)
      (gl/set-viewport gl 0 0 size size)

      (gl/bind rt2 1)
      (gl/draw-with-shader gl quad)
      (gl/unbind rt2)

      (gl/unbind rt1)
      (gl/set-viewport gl 0 0 js/window.innerWidth js/window.innerHeight)
      (.swap this)
      ))
  )
;; ======== FUNCTIONS ====
(defn make-pingpong-buffer
  ([gl simulation]
   (let [buffer (->PingpongBuffer gl simulation 128)]
     (-> buffer (.init))))
  ([gl simulation size]
   (let [buffer (->PingpongBuffer gl simulation size)]
     (-> buffer (.init))))

  )
