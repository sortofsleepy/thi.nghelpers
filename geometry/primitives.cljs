(ns matchstick.geometry.primitives
  (:require [thi.ng.geom.gl.webgl.constants :as glc]
            [thi.ng.geom.gl.core :as gl]
            [thi.ng.typedarrays.core :as arrays]))



(defn create-rect
  "Creates a non-indexed fullscreen plane/quad"
  ([gl]
    (let [modelPrep {:attribs      {:position {:data (arrays/float32 [-1 -1 1 -1 -1 1 1 1]) :size 2}
                                    :uv       {:data (arrays/float32 [0 0 1 0 0 1 1 1]) :size 2}}
                     :mode         glc/triangle-strip
                     :num-vertices 4}]
      (-> modelPrep (gl/make-buffers-in-spec gl glc/dynamic-draw))))
  ([gl shader]
   (let [modelPrep {:attribs      {:position {:data (arrays/float32 [-1 -1 1 -1 -1 1 1 1]) :size 2}
                                   :uv       {:data (arrays/float32 [0 0 1 0 0 1 1 1]) :size 2}}
                    :mode         glc/triangle-strip
                    :shader shader
                    :num-vertices 4}]
     (-> modelPrep (gl/make-buffers-in-spec gl glc/dynamic-draw))))
  )
