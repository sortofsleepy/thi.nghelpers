(ns matchstick.geometry.envmap
  (:require-macros
    [macros.helpers :refer [forloop]])
  (:require
    [thi.ng.geom.vector :refer [vec2 vec3]]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.typedarrays.core :as arrays]
    [matchstick.shaders.envmap :as envshader]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.math.core :as math]))

(def numSegments 24)
(def size 1800)
(def index 0)


(defn getPosition
  ([i j]
   (let [rx (-> (* (/ i numSegments) math/PI)
                (- (* math/PI 0.5)))
         ry (-> (* (/ j numSegments) math/PI 2))
         r size
         t (* r (Math/cos rx))]
     (vec3 (* (Math/cos ry) t) (* (Math/sin rx) r) (* (Math/sin ry) t))
    ))
  )
(defn make-envmap []
  (let [positions #js []
        coords #js []
        indices #js []
        gapUv (/ 1 numSegments)]

    (forloop
      [(i 0) (< i numSegments) (inc i)]
      (forloop
        [(j 0) (< j numSegments) (inc j)]
        (let [x (getPosition i j)
              y (getPosition (+ i 1) j)
              z (getPosition (+ i 1) (+ j 1))
              w (getPosition i (+ j 1))
              u (/ j numSegments)
              v (/ i numSegments)
              c1 (- 1.0 u)
              c2 (+ v gapUv)
              c3 (- 1.0 u gapUv)]

          (.push positions (:x x) (:y x) (:z x))
          (.push positions (:x y) (:y y) (:z y))
          (.push positions (:x z) (:y z) (:z z))
          (.push positions (:x w) (:y w) (:z w))

          (.push coords c1 v c1 c2 c3 c2 c3 v)
          (.push indices (+ (* index 4) 3))
          (.push indices (+ (* index 4) 2))
          (.push indices (+ (* index 4) 0))
          (.push indices (+ (* index 4) 2))
          (.push indices (+ (* index 4) 1))
          (.push indices (+ (* index 4) 0))

          (set! index (inc index))
          ))
      )

    {:positions positions
     :coords coords
     :indices indices}))


;; generates a usuable demo cubemap as utilized by @yiwenl
;; https://github.com/yiwenl/Christmas_Experiment_2015/blob/febb7463311d1ee55467af15d6acffa582681eac/src/js/subsceneTerrain/ViewNightSky.js
(defn generate-envmap [gl]
  (let [mesh-data (make-envmap)
        positions (arrays/float32 (:positions mesh-data))
        uvs (arrays/float32 (:coords mesh-data))
        indices (arrays/uint16 (:indices mesh-data))
        mesh {:attribs {:position {:data positions
                                   :size 3}
                        :uv {:data uvs
                             :size 2}}
              :mode glc/triangles
              :num-items (aget indices "length")
              :indices {:data indices}
              :num-vertices (count (:positions mesh-data))
              :shader (sh/make-shader-from-spec gl (envshader/generateEnvMapShader))}]

    (-> mesh
        (gl/make-buffers-in-spec gl glc/static-draw))))
