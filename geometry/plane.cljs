(ns matchstick.geometry.plane
  (:require
    [thi.ng.typedarrays.core :as tarrays]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.core :as gl]))


(defn CreatePlaneVertices [width height widthSegments heightSegments]
  "Helper function to build attribute data up for a Plane. Returns a map
  of vertices, uvs and indices. Using the Three.js implementation"
  (let [width_half (/ width 2)
        height_half (/ height 2)
        gridX (.floor js/Math widthSegments)
        gridY (.floor js/Math heightSegments)
        gridX1 (inc gridX)
        gridY1 (inc gridY)
        segment_width (/ width gridX)
        segment_height (/ height gridY)
        vertices (tarrays/float32 (* gridX1 gridY1 3))
        normals (tarrays/float32 (* gridX1 gridY1 3))
        uvs (tarrays/float32 (* gridX1 gridY1 2))
        offset (atom 0)
        offset2 (atom 0)
        indices (atom 0)]

    (dotimes [iy gridY1]
      (let [y (- (* iy segment_height) height_half)]
        (dotimes [ix gridX1]
          (let [x (-> (* ix segment_width) (- width_half))
                offsetVal @offset
                offset2Val @offset2]

            (aset vertices offsetVal x)
            (aset vertices (+ offsetVal 1) (* -1 y))
            (aset normals (+ offsetVal 2) 1)
            (aset uvs offset2Val (/ ix gridX))
            (aset uvs (+ offset2Val 1) (- 1 (/ iy gridY)))

            (reset! offset (+ @offset 3))
            (reset! offset2 (+ @offset2 2))))))


    (reset! offset 0)

    (when (> (/ (aget vertices "length") 3) 65535)
      (reset! indices (tarrays/uint32 (* gridX gridY 6))))

    (when (< (/ (aget vertices "length") 3) 65535)
      (reset! indices (tarrays/uint16 (* gridX gridY 6))))

    ;; build indices
    (dotimes [iy gridY]
      (dotimes [ix gridX]
        (let [a (+ ix (* gridX1 iy))
              b (+ ix (* gridX1 (+ iy 1)))
              c (+ (+ ix 1) (* gridX1 (+ iy 1)))
              d (+ (+ ix 1) (* iy gridX1))
              offsetVal @offset
              indicesArr @indices]

          (aset indicesArr offsetVal a)
          (aset indicesArr (+ offsetVal 1) b)
          (aset indicesArr (+ offsetVal 2) d)
          (aset indicesArr (+ offsetVal 3) b)
          (aset indicesArr (+ offsetVal 4) c)
          (aset indicesArr (+ offsetVal 5) d)

          (reset! offset (+ offsetVal 6)))))

    {:vertices vertices
     :uvs uvs
     :normals normals
     :indices @indices}
    ))


(defn make-plane [gl width height wsegments hsegments shader]
  (let [data (CreatePlaneVertices width height wsegments hsegments)
        verts (:vertices data)
        uvs (:uvs data)
        normals (:normals data)
        indices (:indices data)
        spec {:attribs   {:position {:data verts
                                     :size 3}
                          :uv       {:data uvs
                                     :size 2}
                          :normal   {:data normals
                                     :size 3}}
              :mode      glc/triangles
              :num-items (aget indices "length")
              :indices   {:data indices}
              :shader    (sh/make-shader-from-spec gl shader)}
        ]
    (-> spec (gl/make-buffers-in-spec gl glc/dynamic-draw))))