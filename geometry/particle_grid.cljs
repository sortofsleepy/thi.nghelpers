(ns matchstick.geometry.particle-grid
  (:require-macros
    [macros.helpers :refer [forloop]])
  (:require
    [thi.ng.math.core :as math]
    [thi.ng.geom.vector :refer [vec3 vec2]]))

(defn generateGrid
  ([height width depth]
   "Generates a cubed grid of points"
   (let [ret (atom [])
         index (atom 0)
         resolution 8
         cols (/ width resolution)
         rows (/ height resolution)
         depths (/ depth resolution)]
     (forloop
       [(i 0) (< i cols) (inc i)]
       (forloop
         [(j 0) (< j rows) (inc j)]
         (forloop
           [(z 0) (< z depths) (inc z)]
           (let [x (* i resolution)
                 y (* j resolution)
                 w (* z resolution)]

             (swap! ret assoc @index (math/normalize (vec3 x y w)))
             (reset! index (inc @index))))
         )
       ) @ret))


  ([height width]
   "Generates a grid of points"
   (let [ret (atom [])
         index (atom 0)
         resolution 10
         cols (/ width resolution)
         rows (/ height resolution)]
     (forloop
       [(i 0) (< i cols) (inc i)]
       (forloop
         [(j 0) (< j rows) (inc j)]

           (let [x (* i resolution)
                 y (* j resolution)]

             (swap! ret assoc @index (vec3 x y 0))
             (reset! index (inc @index)))
         )

       ) @ret))
  )
