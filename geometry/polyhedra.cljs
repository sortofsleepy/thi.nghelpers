(ns matchstick.geometry.polyhedra
  (:require
    [matchstick.modifiers.explode :as modifier]
    [thi.ng.geom.matrix :refer [M44]]
    [thi.ng.geom.vector :as v :refer [vec3]]
    [thi.ng.math.core :as math]
    [thi.ng.geom.core :as g]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]))

(defn buildIsoData []
  (let [phi 0.618034
        negPhi (* 0.618034 -1)
        positions [ negPhi  1.0  0.0  phi  1.0  0.0  negPhi -1.0  0.0  phi -1.0  0.0
                   0.0  negPhi  1.0  0.0  phi  1.0  0.0  negPhi -1.0  0.0 phi -1.0
                   1.0  0.0  negPhi  1.0  0.0  phi  -1.0  0.0 negPhi  -1.0 0.0  phi ]
        indices (vec (range 0 60))
        sindices [ 0 11  5  0  5  1  0  1  7  0  7 10  0 10 11
                  5 11  4  1  5  9  7  1  8 10  7  6 11 10  2
                  3  9  4  3  4  2  3  2  6  3  6  8  3  8  9
                  4  9  5  2  4 11  6  2 10  8  6  7  9  8  1 ]
        uvs [[    0.000     0.000] [    0.000     1.000] [    1.000     0.000]
             [    0.000     0.000] [    0.000     1.000] [    1.000     0.000]
             [    0.000     0.000] [    0.000     1.000] [    1.000     0.000]
             [    0.000     0.000] [    0.000     1.000] [    1.000     0.000]
             [    0.000     0.000] [    0.000     1.000] [    1.000     0.000]
             [    1.000     0.000] [    0.000     1.000] [    1.000     1.000]
             [    1.000     0.000] [    0.000     1.000] [    1.000     1.000]
             [    1.000     0.000] [    0.000     1.000] [    1.000     1.000]
             [    1.000     0.000] [    0.000     1.000] [    1.000     1.000]
             [    1.000     0.000] [    0.000     1.000] [    1.000     1.000]
             [    1.000     1.000] [    0.000     1.000] [    1.000     0.000]
             [    1.000     1.000] [    0.000     1.000] [    1.000     0.000]
             [    1.000     1.000] [    0.000     1.000] [    1.000     0.000]
             [    1.000     1.000] [    0.000     1.000] [    1.000     0.000]
             [    1.000     1.000] [    0.000     1.000] [    1.000     0.000]
             [    1.000     0.000] [    0.000     1.000] [    0.000     0.000]
             [    1.000     0.000]
             [    0.000     1.000]
             [    0.000     0.000]
             [    1.000     0.000]
             [    0.000     1.000]
             [    0.000     0.000]
             [    1.000     0.000]
             [    0.000     1.000]
             [    0.000     0.000]
             [    1.000     0.000]
             [    0.000     1.000]
             [    0.000     0.000]]
        vertices (atom [])
        normals (atom [])]

    (reset! vertices (map-indexed (fn [i obj]
                                    (let [index (* (get sindices i) 3)
                                          x (get positions index)
                                          y (get positions (+ index 1))
                                          z (get positions (+ index 2))]
                                      (vec3 x y z)
                                      )) (vec (range 0 60))))



    ;; set normals
    ;; TODO is there a more clojure-ish way to do this?
    (let [numTriangles (/ (count indices) 3)]
      (dotimes [i numTriangles]
        (let [index0 (get indices (* i 3))
              index1 (get indices (+ (* i 3) 1))
              index2 (get indices (+ (* i 3) 2))

              v0 (nth @vertices index0)
              v1 (nth @vertices index1)
              v2 (nth @vertices index2)

              e0 (math/- v1 v0)
              e1 (math/- v2 v0)
              normalVal (math/normalize (math/cross e0 e1))]

          (reset! normals (-> @normals
                              (assoc index0 normalVal)
                              (assoc index1 normalVal)
                              (assoc index2 normalVal)))
          )))
    {:vertices @vertices
     :normals @normals
     :indices indices
     :uvs uvs}
    ))

(defn make-explode-iso
  [gl scale shader-spec]
   "Builds a basic Icosahedron based on the Cinder implementation.
   Accepts a scale value as well to make the object larger.
   Provides uvs as well as vertices. Returns a useable model spec for
   rendering."
   (let [isoshape (buildIsoData)
         explo (modifier/explode isoshape)
         modelPrep (-> {:attribs      {:position {:data (arrays/float32 (flatten (:vertices explo))) :size 3}
                                       :uv {:data (arrays/float32 (flatten (:uvs isoshape))) :size 2}
                                       :normal {:data (arrays/float32 (flatten (:normals isoshape))) :size 3}}
                        :mode         glc/triangles
                        :indices      {:data (arrays/uint16 (:indices explo))}
                        :num-items (count (:indices explo))
                        :num-vertices (count (flatten (:vertices explo)))
                        :shader        (sh/make-shader-from-spec gl shader-spec)
                        })
         model (-> modelPrep (gl/make-buffers-in-spec gl glc/static-draw))]
     (println (:indices isoshape))
     ;; replace model matrix with a scaled one
     (-> model  (assoc-in [:uniforms :model] (-> M44 (g/scale scale))))
     ))


(defn make-icosahedron
  ([gl scale]
    "Builds a basic Icosahedron based on the Cinder implementation.
    Accepts a scale value as well to make the object larger.
    Provides uvs as well as vertices. Returns a useable model spec for
    rendering."
    (let [isoshape (buildIsoData)
          modelPrep (-> {:attribs      {:position {:data (arrays/float32 (flatten (:vertices isoshape))) :size 3}
                                        :uv {:data (arrays/float32 (flatten (:uvs isoshape))) :size 2}
                                        :normal {:data (arrays/float32 (flatten (:normals isoshape))) :size 3}}
                         :mode         glc/triangles
                         :indices      {:data (arrays/uint16 (:indices isoshape))}
                         :num-items (count (:indices isoshape))
                         :num-vertices (count (flatten (:vertices isoshape)))
                         ;;:shader        (sh/make-shader-from-spec gl shader-spec)
                         })
          model (-> modelPrep (gl/make-buffers-in-spec gl glc/static-draw))]
      ;; replace model matrix with a scaled one
      (-> model  (assoc-in [:uniforms :model] (-> M44 (g/scale scale))))
      ))

  ([gl scale shader]
   "Builds a basic Icosahedron based on the Cinder implementation.
   Accepts a scale value as well to make the object larger.
   Provides uvs as well as vertices. Returns a useable model spec for
   rendering. This constructor accepts a shader spec as well"
   (let [isoshape (buildIsoData)
         modelPrep (-> {:attribs      {:position {:data (arrays/float32 (flatten (:vertices isoshape))) :size 3}
                                       :uv {:data (arrays/float32 (flatten (:uvs isoshape))) :size 2}
                                       :normal {:data (arrays/float32 (flatten (:normals isoshape))) :size 3}}
                        :mode         glc/triangles
                        :indices      {:data (arrays/uint16 (:indices isoshape))}
                        :num-items (count (:indices isoshape))
                        :num-vertices (count (flatten (:vertices isoshape)))
                        :shader        (sh/make-shader-from-spec gl shader)
                        })
         model (-> modelPrep (gl/make-buffers-in-spec gl glc/static-draw))]

     (-> model  (assoc-in [:uniforms :model] (-> M44 (g/scale scale))))

     ))
  )
